#! /bin/sh

fail_exit() {
    set +x
    ct=1
    while IFS='' read -r line
    do
        printf "%03u - %s\n" $ct "$line"
        (( ct++ ))
    done < ${outfile}
    trap '' 0
    exit 1
} 1>&2

set -x
srcdir=`cd ${top_srcdir}/src && pwd`
tstdir=${PWD}
rcfile="${tstdir}/.complexityrc"
outfile="${tstdir}/complexity.out"
samples=`cd ${srcdir}/../tests  && \
    ls -1 s*mple.c | sed "s@^@$PWD/@"`

cd ${top_builddir}

cat > "$rcfile" <<- _EOF_
	hist
	score
	thresh 0
	_EOF_
trap "rm -f '$rcfile' '${outfile}'" 0
cpx="${PWD}/src/complexity -< $rcfile"

cd ${srcdir}
${cpx} -I main *.c > ${outfile}
cd ${tstdir}

set -e

lc=`grep -E '^Complexity (Scores|Histogram)$' ${outfile} | \
    wc -l`

test $lc -eq 2 || {
    echo "Could not find both titles in output" >&2
    exit 1
}

lc=`grep -E '^ +0.*: +handle_noop' ${outfile} | wc -l`
test $lc -eq 1 || {
    echo "'handle_noop' did not evaluate to zero" >&2
    fail_exit
}

sedcmd='/^[A-Z]/d;/^$/d;/^Highest score:/d;s@ [^ ]*tests/@ @'
$cpx -t0 $samples 2>&1 | \
    sed "$sedcmd" > ${outfile}
sampfile="${tstdir}/complexity.samp"
sed "$sedcmd" > ${sampfile} <<-_EOF_
Complexity Scores
Score | ln-ct | nc-lns| file-name(line): proc-name
    0       1       1   .../tests/sample.c(1): oneline
    0       1       1   .../tests/sample.c(15): tst
    1       1       1   .../tests/sample.c(3): continuesameline
    1       2       2   .../tests/sample.c(20): test
    1       4       3   .../tests/sample.c(7): derefloop

Complexity Histogram
Score-Range  Lin-Ct
    0-9           8 ************************************************************

Scored procedure ct:        5
Non-comment line ct:        8
Average line score:         1
25%-ile score:              0 (75% in higher score procs)
50%-ile score:              1 (half in higher score procs)
75%-ile score:              1 (25% in higher score procs)
Highest score:              1 (continuesameline() in /.../sample.c)
_EOF_
set +e
grep -s -E '^error on ' ${outfile} && \
    fail_exit
cmp ${outfile} ${sampfile} || \
    fail_exit
rm -f ${outfile} ${sampfile}
exit 0
